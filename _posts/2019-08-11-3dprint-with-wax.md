---
layout: single
title:  "Wax Works"
category: wax-works
tags: 
  - beeswax
  - packaging
  - biodegradable 
  - 3d-print
  - life-hacks  
---

Some progress exists on 3d printing with wax. An advantage is that the material can be reused and is biodegradable. Also moulding could be an option to cast packaging.

http://oliviervanherpt.com/3d-printed-beeswax/ or http://www.rooiejoris.nl/3d-printed-beeswax/ or http://www.3ders.org/articles/20140103-3d-printed-beeswax-project.html
[Open-Source Wax RepRap 3-D Printer for Rapid Prototyping Paper-Based Microfluidics ](https://journals.sagepub.com/doi/full/10.1177/2211068215624408)

[How to Make Silicone Molds for Casting Wax Candles](https://www.youtube.com/watch?v=AXmRohKKhcc)

[15 Awesome Wax Hacks](https://www.youtube.com/watch?v=YVOVT6yMK1s)


