---
layout: single
title:  "Ghibli's Kaguya Artwork"
category: Kaguya-artwork
tags: art ghibli animation
---

# Artwork of the Studio Ghibli's Princess Kaguya animation

When I visited Tokyo this spring, I saw an ArtBook of Princess Kaguya. I absolutely love the hand-drawn style of this piece of art, but unfortunately did not decide on buying the artwork book.

I love this running sequence, there are almost no colors and the grass is barely sketched to suggest speed  
<img src="/assets/posts/2018-12-29-kaguya-art/kaguya-running.gif" />

Took the above image from this [pin](https://www.pinterest.ca/pin/67413325656181751/) from Pinterest :)

