---
layout: single
title:  "Floarea-soarelui "
category: poezie
tags: literature radu-stanca
---

de [Radu Stanca](/tags/radu-stanca)

Mă-nvârt, ca floarea-soarelui, pe câmp,  
După făptura ta strălucitoare,  
Iar când apui în zarea călătoare  
Obrazul mi-l aplec către pământ.

Stau noaptea-ntreagă aplecat așa  
Și numai când răsari tu dimineața,  
Descătușând din neguri fruntea grea  
Eu îmi ridic setos spre tine fața.

Sorb razele pe care le trimiți  
Pe câmpul plin de-o harnică speranță,  
Și simt, treptat, cum nervii mei trudiți  
Se umplu de-o frenetică substanță.

Aceasta îmi străbate trupu-ntreg,  
Și-mi urcă prin arterele rebele,  
Până când, împlinit, rodesc și leg  
Cu toate florile ființei mele.