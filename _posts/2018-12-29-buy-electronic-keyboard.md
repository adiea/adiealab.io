---
layout: single
title:  "Electronic Keyboard - Which?"
category: electronic-keyboard
tags: music buyguide to-expand
---

# Research for buying an electronic keyboard

Lately I've been thinking of buying ana electronic keyboard to start learning on this instrument class.

I am also thinking of maybe linking this to my other project, the [PIN radiation detector](/prjs/pin_rad_detector) and generate music.

So now I am oscillating between a digital pian and a arranger or synth. Budget is somewhere near $500, second hand is fine with me if the instrument is in good shape.

Some models I have so far include Yamaha PSR-S670, Yamaha PSR-EW300, Yamaha PSR-EW410, M-Audio Oxygen 88, Yamaha NP-V80 
