---
layout: single
title:  "Transmission oil for the tractor"
category: pasquali906-tractor
tags: buyguide maintenance
---

# Where to buy transmission oil for my tractor

After some online searching these are some places in Bucharest to buy 85W140 transmission oil from:

* [texaco](https://www.emag.ro/ulei-transmisie-texaco-geartex-ep-c-85w140-20-litri-texaco-geartex-ep-c-85w140-20l/pd/D2RD2DBBM/?X-Search-Id=2a6c46d6a7a8dd4b88cb&X-Product-Id=17578163&X-Search-Page=1&X-Search-Position=3&X-Search-Action=view)

* [akcela](https://www.emag.ro/ulei-pentru-transmisie-akcela-gear-135h-ep-sae-85w-140-20-l-1736/pd/D1WHNHBBM/?X-Search-Id=2a6c46d6a7a8dd4b88cb&X-Product-Id=32833946&X-Search-Page=1&X-Search-Position=10&X-Search-Action=view)
