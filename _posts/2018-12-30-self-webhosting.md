---
layout: single
title:  "Self hosting website"
category: this-website
tags: webhosting tools to-expand
---

A very interesting article on [medium](https://medium.com/@kaerumy/hosting-via-vpn-4ddc07721d57) talked about cheap resilient self hosting using VPN technology, own hardware and a CDN front like [cloudflare](https://www.cloudflare.com/). 
In front of the docker containers, VMs or bare workers serving the site, a loadbalancer like [HAProxy](http://www.haproxy.org/) can be used together with a cacher like [Varnish](https://varnish-cache.org/intro/index.html#intro).

A good alternative to OpenVPN could be [SoftEther](https://www.softether.org)