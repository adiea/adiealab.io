---
layout: single
title:  "Dacă ne-am ucide unul pe altul"
category: poezie
tags: 
  - literature 
  - ana-blandiana
---

de [Ana Blandiana](/tags/ana-blandiana)

{% include video id="wzdKnrw06x8" provider="youtube" %}

Dacă ne-am ucide unul pe altul  
Privindu-ne în ochi,  
În ochii noștri în jurul cărora  
Genele stau ca o coroană de spini  
Care-ncununa definitiv  
Orice privire,  
Dacă ne-am ucide, după ce ne-am privit  
Cu dragoste fără de țărm în ochi,  
Și, cunoscându-te, ți-aș spune:  
Mori,  
Mori, dragul meu,  
Va fi atât de bine,  
Vei rămâne numai cu mine,  
Tu, cel născut din cuvânt,  
Vei cunoaște gust de pământ,  
Vei simți ce frumoase sunt rădăcinile  
Împletindu-ți prin ele mâinile,  
Cu nențeleasa bucurie  
De-a nu mai fi pentru vecie…  
Și, mângâindu-mă, mi-ai spune:  
Mori, draga mea,  
Iubita mea cu frunte de octombrie  
Cuprinsă ca-n icoane  
De nimb rotund de moarte,  
Mori,  
Lasă-ți culorile în flori,  
Pletele lungi cărărilor  
Și ochii luciu mărilor,  
Să știi  
De unde să le iei,  
Când vei veni…  
Dac-am muri deodată împreună  
Ucigaș fiecare și victimă,  
Salvator și salvat,  
Privindu-ne fără-ncetare-n ochi,  
Mult după ce nu vom vedea…  

