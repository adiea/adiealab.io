---
layout: single
title:  "LicheePi Zero buildroot image"
category: licheepizero
tags: 
  - embedded 
  - linux
---

Some time ago I backed up a nice little project on indiegogo, the [licheePi Zero](https://www.indiegogo.com/projects/licheepi-zero-6-extensible-linux-module-on-finger#/) a **S**ingle**B**oard**C**omputer featuring Alwinner V3s package, a ARMv7a single core plus 64MB DDRAM that can run Linux.

At that time, you had to use a patched kernel but now almost all the drivers have been [mainlined](http://linux-sunxi.org/Linux_mainlining_effort) so I wanted to make a new image for it using the 4.18 kernel.

I plant to use this for an IoT future project. Because the RAM is limited, I needed to build a minimal linux image, [buildroot](https://buildroot.org/downloads/manual/manual.html) comes to the rescue. Because I am currently on a **Windows** machine, I used their suggested method using vagrant.

> * Install [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
> * Install [Vagrant](https://www.vagrantup.com/downloads.html)

[to do]

https://hackaday.io/project/160360-blueberry-pi
https://github.com/petit-miner/Blueberry-PI/wiki/4.-Buildroot
https://www.reddit.com/r/electronics/comments/83141t/i_made_an_allwinner_v3s_evaluation_board_based/

https://github.com/Varying-Vagrant-Vagrants/VVV/wiki/Connect-to-Your-Vagrant-Virtual-Machine-with-PuTTY

https://github.com/Lichee-Pi/linux/tree/zero-4.14.y/arch/arm/configs
https://github.com/Lichee-Pi/linux/blob/zero-4.14.y/arch/arm/configs/licheepi_zero_defconfig
https://github.com/Squonk42/buildroot-licheepi-zero/commit/b99e0c078d56954b5b4f8d07980881d6b7b40530
https://github.com/Squonk42/buildroot-licheepi-zero/commit/abe975d2dbfc331343a2c01bfc6e21ef88beac64
https://hackaday.io/project/134065-funkey-zero/log/144687-screen-is-working
https://hackaday.io/project/134065-funkey-zero/log/144796-linux-distribution
https://hackaday.io/project/134065-funkey-zero/log/146119-emc-emi-rfi-esd

http://bbs.sipeed.com/t/let-s-design-lichee-pi-zero-and-it-s-dock/27
http://bbs.sipeed.com/t/how-to-setup-build-environment/90

https://docs.huihoo.com/linux/kernel/a2/
https://github.com/umiddelb/armhf/wiki/How-To-compile-a-custom-Linux-kernel-for-your-ARM-device
https://elinux.org/images/2/2a/Schulz-how-to-support-new-board-u-boot-linux.pdf
https://lwn.net/Articles/608945/

http://linux-sunxi.org/USB_Gadget/Ethernet
https://learn.adafruit.com/turning-your-raspberry-pi-zero-into-a-usb-gadget/overview
https://gist.github.com/gbaman/975e2db164b3ca2b51ae11e45e8fd40a
http://linux-sunxi.org/USB_Gadget/Serial
https://forum.armbian.com/topic/4814-orange-pi-one-usb-otg/
https://www.digi.com/resources/documentation/digidocs/90001546/reference/bsp/r_usb_device.htm