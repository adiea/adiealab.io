---
layout: single
title:  "Old recipes book"
category: food
tags: old foodrecipes [post-idea]
---

Found an interesting article on [Ierburi Uitate](https://ierburiuitate.wordpress.com/2016/11/11/stiinta-bucatarului/) about an old book of recipes from Transilvania. Here is the [pdf issue](/assets/posts/2018-12-30-old-recipes/apostrof-2013-02.pdf) of the [Apostrof](http://revista-apostrof.ro) magazine related to this.